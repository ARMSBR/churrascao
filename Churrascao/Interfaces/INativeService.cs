﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Churrascao.Interfaces
{
    public interface INativeService
    {
        bool CheckPermissions();
        void RequestPermissions();
    }
}