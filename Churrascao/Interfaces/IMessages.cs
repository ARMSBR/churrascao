﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace Churrascao.Interfaces
{
    public interface IMessages
    {
        void DisplayMesssage(string title, string message, bool longLength);

        void DisplayMesssage(string message, bool longLength = true);

        void DisplayNotification(string title, string message, string icon);

        void DisplayNotification(string title, string message);

        void DisplayNotification(string message);
    }
}
