﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Xamarin.Forms;
using Churrascao.Interfaces;
using Churrascao.Models;

namespace Churrascao.Database
{
    public class AlunoDataAccess
    {
        SQLiteConnection conexao;

        public AlunoDataAccess()
        {
            conexao = DependencyService.Get<IDatabase>().GetConnection();
            conexao.CreateTable<Aluno>();
        }

        public List<Aluno> GetAlunos()
        {
            return conexao.Table<Aluno>().ToList();
        }

        public int InsertAluno(Aluno novo)
        {
            return conexao.Insert(novo);
        }

        public int UpdateAluno(Aluno novo)
        {
            return conexao.Update(novo);
        }

        public int DeleteAluno(Aluno novo)
        {
            return conexao.Delete(novo);
        }

        public int DeleteTodosAluno()
        {
            return conexao.DeleteAll<Aluno>();
        }
    }
}
