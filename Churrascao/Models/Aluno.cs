﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace Churrascao.Models
{
    [Table("Aluno")]
    public class Aluno
    {
        [PrimaryKey, Unique, AutoIncrement]
        public int Id { get; set; }
        public int IdSite { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string UsernameCanonical { get; set; }
        public string EmailCanonical { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string Name { get; set; }
        public string Foto { get; set; }
        public string Turma { get; set; }
        public int TurmaId { get; set; }
        public DateTime DataNascimento { get; set; }
        public DateTime DataCriacao { get; set; }

    }
}