﻿using Churrascao.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using Churrascao.Classes;
using Churrascao.Views;

namespace Churrascao.ViewModels
{
    public class CreateParticipanteViewModel : INotifyPropertyChanged
    {
        public INavigation Navigation { get; set; }
        public ICommand CommandInsert { get; private set; }

        public ICommand CommandDelete { get; private set; }

        public CreateParticipanteViewModel(INavigation navigation)
        {
            this.Navigation = navigation;
            CommandDelete = new Command(ExcluirParticipante);
            CommandInsert = new Command(InserirParticipante);
        }


        string textBTN = "Cadastrar";
        int idBBQ = 0;
        string nome = "";
        bool confirmado;
        string valorPago;
        string idChurrasco;
        string idParticipant;
        bool isEditing = false;
        public string Nome
        {
            get
            {
                return nome;
            }
            set
            {
                nome = value;
                NotifyPropertyChanged("Nome");
            }
        }
        public bool Confirmado
        {
            get
            {
                return confirmado;
            }
            set
            {
                confirmado = value;
                NotifyPropertyChanged("Confirmado");
            }
        }
        public string ValorPago
        {
            get
            {
                return valorPago;
            }
            set
            {
                valorPago = value;
                NotifyPropertyChanged("ValorPago");
            }
        }

        public string TextBTN
        {
            get
            {
                return textBTN;
            }
            set
            {
                textBTN = value;
                NotifyPropertyChanged("TextBTN");
            }
        }

        public bool IsEditing
        {
            get
            {
                return isEditing;
            }
            set
            {
                isEditing = value;
                NotifyPropertyChanged("IsEditing");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void ExcluirParticipante()
        {
            Connection conexao = new Connection();
            conexao.BuildHttpClient();
            string token = "", idChurrasco = "";

            token = Application.Current.Properties["tokenConnection"].ToString();
            idChurrasco = Application.Current.Properties["idChurrasco"].ToString();
            DeleteResponse deleteParticipant = conexao.CallDeleteParticipante<DeleteResponse>(token, idParticipant);
            if (deleteParticipant == null)
            {
                App.Current.MainPage.DisplayAlert("Erro", "Não foi possível deletar o participante", "OK");
            }
            else
            {
                App.Current.MainPage.DisplayAlert("Deletado", "Participante deletado com sucesso", "OK");
                Navigation.PopAsync();
            }

        }

        public void InserirParticipante()
        {
            Connection conexao = new Connection();
            conexao.BuildHttpClient();
            string token = "", idChurrasco = "";

            token = Application.Current.Properties["tokenConnection"].ToString();
            idChurrasco = Application.Current.Properties["idChurrasco"].ToString();
            if (isEditing == false)
            {
                string valorConfirmado = Confirmado == true ? "true" : "false";
                ChurrascoResponse createParticipant = conexao.CallCreateParticipante<ChurrascoResponse>(token, new string[] { idChurrasco, Nome, valorConfirmado, ValorPago });
                if (createParticipant == null)
                {
                    App.Current.MainPage.DisplayAlert("Erro", "Não foi possível adicionar um participante", "OK");
                }
                else
                {
                    App.Current.MainPage.DisplayAlert("Adicionado", "Participante adicionado com sucesso", "OK");
                    Navigation.PopAsync();
                }
            }
            else
            {
                string valorConfirmado = Confirmado == true ? "true" : "false";
                ChurrascoResponse createParticipant = conexao.CallUpdateParticipante<ChurrascoResponse>(token, new string[] { idParticipant, idChurrasco, Nome, valorConfirmado, ValorPago });
                if (createParticipant == null)
                {
                    App.Current.MainPage.DisplayAlert("Erro", "Não foi possível alterar o participante", "OK");
                }
                else
                {
                    Application.Current.Properties["idParticipant"] = "";
                    Application.Current.Properties["nameParticipant"] = "";
                    Application.Current.Properties["confirmedParticipant"] = "";
                    Application.Current.Properties["valueParticipant"] = "";
                    Application.Current.Properties["idChurrascoParticipant"] = "";
                    App.Current.MainPage.DisplayAlert("Editado", "Participante alterado com sucesso", "OK");
                    Navigation.PopAsync();
                }
            }
        }
        public void LoadParticipant()
        {
            string token = "";
            string confirmed = Application.Current.Properties["confirmedParticipant"].ToString();
            token = Application.Current.Properties["tokenConnection"].ToString();
            Nome = Application.Current.Properties["nameParticipant"].ToString();
            Confirmado = Application.Current.Properties["confirmedParticipant"].ToString().ToLower() == "true" ? true : false;
            ValorPago = Application.Current.Properties["valueParticipant"].ToString();
            idChurrasco = Application.Current.Properties["idChurrascoParticipant"].ToString();
            idParticipant = Application.Current.Properties["idParticipant"].ToString();
            if (!string.IsNullOrEmpty(Nome))
            {
                IsEditing = true;
                TextBTN = "Atualizar";
            }
        }
    }
}
