﻿using Churrascao.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using Churrascao.Classes;
using Churrascao.Views;

namespace Churrascao.ViewModels
{
    public class ChurrascoViewModel : INotifyPropertyChanged
    {
        public INavigation Navigation { get; set; }
        public ICommand CommandAdicionarParticipant { get; private set; }
        public ICommand CommandDeletarChurrasco { get; private set; }

        GetChurrasco listaChurrascos;

        string titulo = "";
        string descricao = "";
        string data = "";
        double valor;
        public string Titulo
        {
            get
            {
                return titulo;
            }
            set
            {
                titulo = value;
                NotifyPropertyChanged("Titulo");
            }
        }

        public string Descricao
        {
            get
            {
                return descricao;
            }
            set
            {
                descricao = value;
                NotifyPropertyChanged("Descricao");
            }
        }

        public string Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
                NotifyPropertyChanged("Data");
            }
        }

        public double Valor
        {
            get
            {
                return valor;
            }
            set
            {
                valor = value;
                NotifyPropertyChanged("Valor");
            }
        }

        public List<ParticipantResponse> participants;
        public List<ParticipantResponse> Participants
        {
            get
            {
                return participants;
            }
            set
            {
                participants = value;
                NotifyPropertyChanged("Participants");
            }
        }

        public ParticipantResponse participantSelectedItem;
        public ParticipantResponse ParticipantSelectedItem
        {
            get
            {
                return participantSelectedItem;
            }
            set
            {
                participantSelectedItem = value;
                if (value != null)
                {
                    LoadParticipantItem();
                }
                NotifyPropertyChanged("ChurrascoSelectedItem");
            }
        }
               

        public ChurrascoViewModel(INavigation navigation)
        {
            this.Navigation = navigation;
            CommandAdicionarParticipant = new Command(AdicionarParticipant);
            CommandDeletarChurrasco = new Command(DeletarChurrasco);
        }

        public void AdicionarParticipant()
        {
            Application.Current.Properties["idParticipant"] = "";
            Application.Current.Properties["nameParticipant"] = "";
            Application.Current.Properties["confirmedParticipant"] = "";
            Application.Current.Properties["valueParticipant"] = "";
            Application.Current.Properties["idChurrascoParticipant"] = "";
            Navigation.PushAsync(new CreateParticipanteView());
        }

        public void DeletarChurrasco()
        {
            Connection conexao = new Connection();
            conexao.BuildHttpClient();
            string token = "", idChurrasco = "";
            
            token = Application.Current.Properties["tokenConnection"].ToString();
            idChurrasco = Application.Current.Properties["idChurrasco"].ToString();
            DeleteResponse deleteChurrasco = conexao.CallDeleteChurrasco<DeleteResponse>(token, idChurrasco);
            if (deleteChurrasco == null)
            {
                App.Current.MainPage.DisplayAlert("Erro", "Não foi possível deletar nenhum churrasco", "OK");
            }
            else
            {
                App.Current.MainPage.DisplayAlert("Deletado", "Churrasco deletado com sucesso", "OK");
                Navigation.PopAsync();
            }
        }

        public void LoadChurrasco()
        {
            Connection conexao = new Connection();
            conexao.BuildHttpClient();
            string token = "", idChurrasco = "";
            
            token = Application.Current.Properties["token"].ToString();
            idChurrasco = Application.Current.Properties["idChurrasco"].ToString();

            listaChurrascos = conexao.CallLoadChurrasco<GetChurrasco>(token, idChurrasco);               
            if(listaChurrascos == null)
            {
                App.Current.MainPage.DisplayAlert("Erro", "Não foi possível encontrar nenhum churrasco", "OK");
            }
            else
            {
                Titulo = listaChurrascos.Title;
                Descricao = listaChurrascos.Description;
                Data = listaChurrascos.Date;
                Valor = listaChurrascos.ValuePerson;
                Participants = listaChurrascos.Participants;
            }
        }

        public async void LoadParticipantItem()
        {
            Application.Current.Properties["idParticipant"] = ParticipantSelectedItem.id;
            Application.Current.Properties["nameParticipant"] = ParticipantSelectedItem.Name;
            Application.Current.Properties["confirmedParticipant"] = ParticipantSelectedItem.Confirmed;
            Application.Current.Properties["valueParticipant"] = ParticipantSelectedItem.Value;
            Application.Current.Properties["idChurrascoParticipant"] = listaChurrascos.id;
            await Application.Current.SavePropertiesAsync();
            Navigation.PushAsync(new CreateParticipanteView());
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
