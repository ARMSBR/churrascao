﻿using Churrascao.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using Churrascao.Classes;
using Churrascao.Views;

namespace Churrascao.ViewModels
{
    public class CreateChurrascoViewModel : INotifyPropertyChanged
    {
        public INavigation Navigation { get; set; }
        public ICommand CommandInsert { get; private set; }

        public CreateChurrascoViewModel(INavigation navigation)
        {
            this.Navigation = navigation;
            CommandInsert = new Command(InserirChurrasco);
        }

        string titulo = "";
        string descricao = "";
        DateTime data = DateTime.Now;
        TimeSpan hora = new TimeSpan();
        string valor;
        public string Titulo
        {
            get
            {
                return titulo;
            }
            set
            {
                titulo = value;
                NotifyPropertyChanged("Titulo");
            }
        }

        public string Descricao
        {
            get
            {
                return descricao;
            }
            set
            {
                descricao = value;
                NotifyPropertyChanged("Descricao");
            }
        }

        public DateTime Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
                NotifyPropertyChanged("Data");
            }
        }

        public TimeSpan Hora
        {
            get
            {
                return hora;
            }
            set
            {
                hora = value;
                NotifyPropertyChanged("Hora");
            }
        }

        public string Valor
        {
            get
            {
                return valor;
            }
            set
            {
                valor = value;
                NotifyPropertyChanged("Valor");
            }
        }

        public void InserirChurrasco()
        {
            Connection conexao = new Connection();
            conexao.BuildHttpClient();
            if (string.IsNullOrEmpty(Titulo) || string.IsNullOrEmpty(Descricao) || Data == null || Hora == null)
            {
                App.Current.MainPage.DisplayAlert("Erro Inserir", "Preencha todos os dados para cadastrar um churrasco", "OK");
            }
            else
            {
                string token = Application.Current.Properties["token"].ToString();
                string dateAndTime = Data.Year.ToString() +"-"+ Data.Month.ToString() + "-" + Data.Day.ToString() + " " + Hora.ToString();
                ChurrascoResponse churrascoResponse =
                    conexao.CallCreateChurrasco<ChurrascoResponse>(token, new string[] { Titulo, Descricao, dateAndTime, Valor.ToString() });
                if (churrascoResponse == null || string.IsNullOrEmpty(churrascoResponse.message))
                {
                    App.Current.MainPage.DisplayAlert("Erro Login", "Não foi possível logar com essas credenciais", "OK");
                    return;
                }
                else
                {
                    Navigation.PushAsync(new CreateChurrascoView());
                }
                int idBBQ = 0;
               // Navigation.PushAsync(new CreateParticipanteView(idBBQ));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
