﻿using Churrascao.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using Churrascao.Classes;
using Churrascao.Views;

namespace Churrascao.ViewModels
{
    public class ListaChurrascosViewModel : INotifyPropertyChanged
    {
        public INavigation Navigation { get; set; }
        public ICommand CommandAdicionarChurrasco { get; private set; }

        public List<Churrasco> churrascos;
        public List<Churrasco> Churrascos
        {
            get
            {
                return churrascos;
            }
            set
            {
                churrascos = value;
                NotifyPropertyChanged("Churrascos");
            }
        }

        public Churrasco churrascoSelectedItem;
        public Churrasco ChurrascoSelectedItem
        {
            get
            {
                return churrascoSelectedItem;
            }
            set
            {
                churrascoSelectedItem = value;
                if (value != null)
                {
                    LoadChurrascoItem();
                }
                NotifyPropertyChanged("ChurrascoSelectedItem");
            }
        }
               

        public ListaChurrascosViewModel(INavigation navigation)
        {
            this.Navigation = navigation;
            CommandAdicionarChurrasco = new Command(AdicionarChurrasco);
        }

        public void AdicionarChurrasco()
        {
            Navigation.PushAsync(new CreateChurrascoView());
        }

        public void LoadChurrascos()
        {
            Connection conexao = new Connection();
            conexao.BuildHttpClient(); 
            string token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            ListaChurrasco listaChurrascos = conexao.CallListaChurrascos<ListaChurrasco>(token);               
            if(listaChurrascos == null || listaChurrascos.data.Count <=0)
            {
                App.Current.MainPage.DisplayAlert("Erro", "Não foi possível encontrar nenhum churrasco", "OK");
            }
            else
            {
                Churrascos = listaChurrascos.data;
            }
        }

        public async void LoadChurrascoItem()
        {
            Application.Current.Properties["idChurrasco"] = ChurrascoSelectedItem.id;
            await  Application.Current.SavePropertiesAsync();
            await Navigation.PushAsync(new ChurrascoView());
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
