﻿using Churrascao.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using Churrascao.Classes;
using Churrascao.Views;

namespace Churrascao.ViewModels
{
    public class TelaLoginViewModel : INotifyPropertyChanged
    {
        public INavigation Navigation { get; set; }
        public ICommand CommandLogin { get; private set; }

        public TelaLoginViewModel(INavigation navigation)
        {
            this.Navigation = navigation;
            CommandLogin = new Command(RealizarLogin);

            service = DependencyService.Get<INativeService>();
        }

        string login = "";
        string senha = "";
        public string Login
        {
            get
            {
                return login;
            }
            set
            {
                login = value;
                NotifyPropertyChanged("Login");
            }
        }

        public string Senha
        {
            get
            {
                return senha;
            }
            set
            {
                senha = value;
                NotifyPropertyChanged("Senha");
            }
        }

        INativeService service { get; set; }
        bool PermissionsGranted { get; set; }

        public async void RealizarLogin()
        {
            PermissionsGranted = service.CheckPermissions();

            if (PermissionsGranted == false)
            { 
                App.Current.MainPage.DisplayAlert("Aviso", "É necessário aceitar as permissões do app para fazer login", "OK");
                service.RequestPermissions();
                PermissionsGranted = service.CheckPermissions();
                return;
            }
            Connection conexao = new Connection();
            conexao.BuildHttpClient();
            if (string.IsNullOrEmpty(Login) || string.IsNullOrEmpty(Senha))
            {
                App.Current.MainPage.DisplayAlert("Erro Login", "Preencha todos os dados para logar", "OK");
            }
            else
            {
                LoginResponse loginAluno = conexao.CallLogin<LoginResponse>(new string[] { Login, Senha });               
                if (loginAluno == null || string.IsNullOrEmpty(loginAluno.token))
                {
                    App.Current.MainPage.DisplayAlert("Erro Login", "Não foi possível logar com essas credenciais", "OK");
                    return;
                }
                else
                {
                    Application.Current.Properties["tokenConnection"] = loginAluno.token;                    
                    await Application.Current.SavePropertiesAsync();
                    await Navigation.PushAsync(new ListaChurrascosView());
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
