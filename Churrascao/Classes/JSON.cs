﻿namespace Churrascao.Classes
{
    public class JSON
    {

        public string GetLogin()
        {
            return "https://trinca-api.herokuapp.com/bbq/auth";
        }

        public string GetChurrascos()
        {
            return "https://trinca-api.herokuapp.com/bbq/";
        }

        public string GetChurrasco()
        {
            return "https://trinca-api.herokuapp.com/bbq/";
        }

        public string PostChurrasco()
        {
            return "https://trinca-api.herokuapp.com/bbq/";
        }

        public string PostParticipante()
        {
            return "https://trinca-api.herokuapp.com/bbq/participant";
        }
    }
}
