﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Churrascao.Classes
{
    public class GetChurrasco
    {
        public int id;
        public string title;
        public string description;
        public string date;
        public double value_per_person;
        List<ParticipantResponse> participants;

        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public string Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
            }
        }

        public double ValuePerson
        {
            get
            {
                return value_per_person;
            }
            set
            {
                value_per_person = value;
            }
        }

        public List<ParticipantResponse> Participants
        {
            get
            {
                return participants;
            }
            set
            {
                participants = value;
            }
        }
    }
}
