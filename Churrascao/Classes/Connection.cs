﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Churrascao.Classes
{
    public class Connection
    {
        private HttpClient httpClient;
        public Connection()
        {

        }

        public virtual void Initialize()
        {
            BuildHttpClient();
        }

        public virtual void BuildHttpClient()
        {
            NetworkCredential NetworkCredential = new NetworkCredential();

            /*if (!string.IsNullOrEmpty(Options.Username) && !string.IsNullOrEmpty(Options.Password))
            {
                NetworkCredential.UserName = Options.Username;
                NetworkCredential.Password = Options.Password;
            }*/

            HttpClientHandler handler = new HttpClientHandler()
            {
                UseProxy = false,
                ClientCertificateOptions = ClientCertificateOption.Automatic,
                Credentials = NetworkCredential,
                //AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };

            this.httpClient = new HttpClient(handler);

            /*if (Options.Timeout != TimeSpan.Zero)
                this.httpClient.Timeout = Options.Timeout;*/

            JSON json = new JSON();
            this.httpClient.BaseAddress = new Uri(json.GetLogin());

            /*if (!string.IsNullOrEmpty(Options.Username) && !string.IsNullOrEmpty(Options.Password))
            {
                var byteArray = System.Text.Encoding.UTF8.GetBytes(Options.Username + ":" + Options.Password);
                this.httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            }*/

            //this.httpClient.DefaultRequestHeaders.AcceptEncoding.Add(new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));
            //this.httpClient.DefaultRequestHeaders.AcceptEncoding.Add(new System.Net.Http.Headers.StringWithQualityHeaderValue("deflate"));
        }
        public virtual T Call<T>(Enum methodEnum, object parameter)
        {
            string methodName = Convert.ToString(methodEnum);
            Uri uri = new Uri(this.httpClient.BaseAddress + "15.json");// + methodName);
            string requestContent = string.Empty;

            //if (parameter != null)
             //   requestContent = Newtonsoft.Json.JsonConvert.SerializeObject(parameter);

            StringContent requestMessage = new StringContent(
                requestContent,
                Encoding.UTF8,
                "application/json");

            /*var formContent =
                new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("_format", "json"),
                    new KeyValuePair<string, string>("login", "alcidesarms@yahoo.com.br"),
                    new KeyValuePair<string, string>("password", "123")
                });
                */

            var formContent =
                new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("_format", "json"),
                    new KeyValuePair<string, string>("alunoId", "15")
                });

            this.httpClient.DefaultRequestHeaders.Date = DateTime.Now;

            HttpResponseMessage responseMessage = null;
            /*if (Options.Timeout != TimeSpan.Zero)
            {
                this.httpClient.Timeout = Options.Timeout;
            }*/
            try
            {
                responseMessage = this.httpClient.PostAsync(uri, formContent).Result;            
                responseMessage.EnsureSuccessStatusCode();
            }
            catch (Exception comException)
            {                
                /*log.Error(comException.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
            }

            try
            {
                var responseText = responseMessage.Content.ReadAsStringAsync().Result;
                T convertedType = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText);

                return convertedType;
            }
            catch (Exception ex)
            {
                /*log.Error(ex.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(null);
            }
        }

        public virtual T CallLogin<T>(string[] parameter)
        {
            JSON json = new JSON();
            this.httpClient.BaseAddress = new Uri(json.GetLogin());

            Uri uri = new Uri(this.httpClient.BaseAddress + "");
            string requestContent = string.Empty;
            
            StringContent requestMessage = new StringContent(
                requestContent,
                Encoding.UTF8,
                "application/json");

            var formContent =
                new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("_format", "json"),
                    new KeyValuePair<string, string>("username", parameter[0]),
                    new KeyValuePair<string, string>("password", parameter[1])
                });
                                

            this.httpClient.DefaultRequestHeaders.Date = DateTime.Now;

            HttpResponseMessage responseMessage = null;
            try
            {
                responseMessage = this.httpClient.PostAsync(uri, formContent).Result;
                responseMessage.EnsureSuccessStatusCode();
            }
            catch (Exception comException)
            {
                /*log.Error(comException.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
            }

            try
            {
                var responseText = responseMessage.Content.ReadAsStringAsync().Result;
                T convertedType = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText);

                return convertedType;
            }
            catch (Exception ex)
            {
                /*log.Error(ex.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(null);
            }
        }

        public virtual T CallCreateChurrasco<T>(string token, string[] parameter)
        {
            JSON json = new JSON();
            this.httpClient.BaseAddress = new Uri(json.PostChurrasco());

            Uri uri = new Uri(this.httpClient.BaseAddress + "");
            string requestContent = string.Empty;

            StringContent requestMessage = new StringContent(
                requestContent,
                Encoding.UTF8,
                "application/json");

            var formContent =
                new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("_format", "json"),
                    new KeyValuePair<string, string>("title", parameter[0]),
                    new KeyValuePair<string, string>("description", parameter[1]),
                    new KeyValuePair<string, string>("date", parameter[2]),
                    new KeyValuePair<string, string>("value_per_person", parameter[3])
                });

            this.httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            this.httpClient.DefaultRequestHeaders.Date = DateTime.Now;

            HttpResponseMessage responseMessage = null;
            try
            {
                responseMessage = this.httpClient.PostAsync(uri, formContent).Result;
                responseMessage.EnsureSuccessStatusCode();
            }
            catch (Exception comException)
            {
                /*log.Error(comException.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
            }

            try
            {
                var responseText = responseMessage.Content.ReadAsStringAsync().Result;
                T convertedType = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText);

                return convertedType;
            }
            catch (Exception ex)
            {
                /*log.Error(ex.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(null);
            }
        }

        public virtual T CallCreateParticipante<T>(string token, string[] parameter)
        {
            JSON json = new JSON();
            this.httpClient.BaseAddress = new Uri(json.PostParticipante());

            Uri uri = new Uri(this.httpClient.BaseAddress + "");
            string requestContent = string.Empty;

            StringContent requestMessage = new StringContent(
                requestContent,
                Encoding.UTF8,
                "application/json");

            var formContent =
                new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("_format", "json"),
                    new KeyValuePair<string, string>("bbq_id", parameter[0]),
                    new KeyValuePair<string, string>("name", parameter[1]),
                    new KeyValuePair<string, string>("confirmed", parameter[2]),
                    new KeyValuePair<string, string>("value_paid", parameter[3])
                });

            this.httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            this.httpClient.DefaultRequestHeaders.Date = DateTime.Now;

            HttpResponseMessage responseMessage = null;
            try
            {
                responseMessage = this.httpClient.PostAsync(uri, formContent).Result;
                responseMessage.EnsureSuccessStatusCode();
            }
            catch (Exception comException)
            {
                /*log.Error(comException.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
            }

            try
            {
                var responseText = responseMessage.Content.ReadAsStringAsync().Result;
                T convertedType = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText);

                return convertedType;
            }
            catch (Exception ex)
            {
                /*log.Error(ex.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(null);
            }
        }

        public virtual T CallUpdateParticipante<T>(string token, string[] parameter)
        {
            JSON json = new JSON();
            this.httpClient.BaseAddress = new Uri(json.PostParticipante());

            Uri uri = new Uri(this.httpClient.BaseAddress + "");
            string requestContent = string.Empty;

            StringContent requestMessage = new StringContent(
                requestContent,
                Encoding.UTF8,
                "application/json");

            var formContent =
                new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("_format", "json"),
                    new KeyValuePair<string, string>("id", parameter[0]),
                    new KeyValuePair<string, string>("bbq_id", parameter[0]),
                    new KeyValuePair<string, string>("name", parameter[1]),
                    new KeyValuePair<string, string>("confirmed", parameter[2]),
                    new KeyValuePair<string, string>("value_paid", parameter[3])
                });

            this.httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            this.httpClient.DefaultRequestHeaders.Date = DateTime.Now;

            HttpResponseMessage responseMessage = null;
            try
            {
                responseMessage = this.httpClient.PutAsync(uri, formContent).Result;
                responseMessage.EnsureSuccessStatusCode();
            }
            catch (Exception comException)
            {
                /*log.Error(comException.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
            }

            try
            {
                var responseText = responseMessage.Content.ReadAsStringAsync().Result;
                T convertedType = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText);

                return convertedType;
            }
            catch (Exception ex)
            {
                /*log.Error(ex.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(null);
            }
        }

        public virtual T CallLoadChurrasco<T>(string token, string idChurrasco)
        {
            JSON json = new JSON();
            this.httpClient.BaseAddress = new Uri(json.GetChurrasco() + idChurrasco);

            Uri uri = new Uri(this.httpClient.BaseAddress + "");
            string requestContent = string.Empty;

            StringContent requestMessage = new StringContent(
                requestContent,
                Encoding.UTF8,
                "application/json");

            var formContent =
                new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("_format", "json"),
                });

            this.httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            this.httpClient.DefaultRequestHeaders.Date = DateTime.Now;

            HttpResponseMessage responseMessage = null;
            try
            {
                responseMessage = this.httpClient.GetAsync(uri).Result;
                responseMessage.EnsureSuccessStatusCode();
            }
            catch (Exception comException)
            {
                /*log.Error(comException.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
            }

            try
            {
                var responseText = responseMessage.Content.ReadAsStringAsync().Result;
                T convertedType = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText);

                return convertedType;
            }
            catch (Exception ex)
            {
                /*log.Error(ex.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(null);
            }
        }

        public virtual T CallDeleteChurrasco<T>(string token, string idChurrasco)
        {
            JSON json = new JSON();
            this.httpClient.BaseAddress = new Uri(json.GetChurrasco() + idChurrasco);

            Uri uri = new Uri(this.httpClient.BaseAddress + "");
            string requestContent = string.Empty;

            StringContent requestMessage = new StringContent(
                requestContent,
                Encoding.UTF8,
                "application/json");

            var formContent =
                new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("_format", "json"),
                });

            this.httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            this.httpClient.DefaultRequestHeaders.Date = DateTime.Now;

            HttpResponseMessage responseMessage = null;
            try
            {
                responseMessage = this.httpClient.DeleteAsync(uri).Result;
                responseMessage.EnsureSuccessStatusCode();
            }
            catch (Exception comException)
            {
                /*log.Error(comException.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
            }

            try
            {
                var responseText = responseMessage.Content.ReadAsStringAsync().Result;
                T convertedType = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText);

                return convertedType;
            }
            catch (Exception ex)
            {
                /*log.Error(ex.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(null);
            }
        }

        public virtual T CallDeleteParticipante<T>(string token, string idChurrasco)
        {
            JSON json = new JSON();
            this.httpClient.BaseAddress = new Uri(json.PostParticipante() +"/" + idChurrasco);

            Uri uri = new Uri(this.httpClient.BaseAddress + "");
            string requestContent = string.Empty;

            StringContent requestMessage = new StringContent(
                requestContent,
                Encoding.UTF8,
                "application/json");

            var formContent =
                new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("_format", "json"),
                });

            this.httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            this.httpClient.DefaultRequestHeaders.Date = DateTime.Now;

            HttpResponseMessage responseMessage = null;
            try
            {
                responseMessage = this.httpClient.DeleteAsync(uri).Result;
                responseMessage.EnsureSuccessStatusCode();
            }
            catch (Exception comException)
            {
                /*log.Error(comException.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
            }

            try
            {
                var responseText = responseMessage.Content.ReadAsStringAsync().Result;
                T convertedType = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText);

                return convertedType;
            }
            catch (Exception ex)
            {
                /*log.Error(ex.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(null);
            }
        }

        public virtual T CallListaChurrascos<T>(string token)
        {
            JSON json = new JSON();
            this.httpClient.BaseAddress = new Uri(json.GetChurrascos());

            Uri uri = new Uri(this.httpClient.BaseAddress + "");
            string requestContent = string.Empty;

            StringContent requestMessage = new StringContent(
                requestContent,
                Encoding.UTF8,
                "application/json");

            var formContent =
                new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("_format", "json"),
                });

            this.httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            this.httpClient.DefaultRequestHeaders.Date = DateTime.Now;

            HttpResponseMessage responseMessage = null;
            try
            {
                responseMessage = this.httpClient.GetAsync(uri).Result;
                responseMessage.EnsureSuccessStatusCode();
            }
            catch (Exception comException)
            {
                /*log.Error(comException.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
            }

            try
            {
                var responseText = responseMessage.Content.ReadAsStringAsync().Result;
                T convertedType = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText);

                return convertedType;
            }
            catch (Exception ex)
            {
                /*log.Error(ex.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(null);
            }
        }

        public virtual void CallAsync<T>(Enum methodEnum, object parameter, Action<T> result)
        {
            string methodName = Convert.ToString(methodEnum);
            Uri uri = new Uri(this.httpClient.BaseAddress + methodName);
            string requestContent = string.Empty;

            if (parameter != null)
                requestContent = Newtonsoft.Json.JsonConvert.SerializeObject(parameter);

            StringContent requestMessage = new StringContent(
                requestContent,
                Encoding.UTF8,
                "application/json");
            this.httpClient.DefaultRequestHeaders.Date = DateTime.Now;

            HttpResponseMessage responseMessage;
            /*if (Options.Timeout != TimeSpan.Zero)
            {
                this.httpClient.Timeout = Options.Timeout;
            }*/

            this.httpClient.PostAsync(uri, requestMessage).ContinueWith(
                (postTask) =>
                {
                    try
                    {
                        responseMessage = postTask.Result;
                        responseMessage.EnsureSuccessStatusCode();
                        string responseText = responseMessage.Content.ReadAsStringAsync().Result;

                        T convertedType = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText);

                        result(convertedType);
                    }
                    catch (Exception comException)
                    {
                        /*log.Error(comException.ToString());
                        log.Error("HTTPCLIENT URL: " + uri);*/
                    }
                });
        }

        public virtual byte[] GetByteArrayAsync(string urlContent)
        {
            byte[] resultArray = null;
            this.httpClient.Timeout = new TimeSpan(0, 1, 0);

            Uri uri = new Uri(this.httpClient.BaseAddress + urlContent);
            var response = this.httpClient.GetAsync(uri).Result;

            try
            {
                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    resultArray = response.Content.ReadAsByteArrayAsync().Result;
                }
            }
            catch (Exception ex)
            {
                /*log.Error(ex.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + response.StatusCode);*/
            }

            return resultArray;
        }
        
        public virtual T Call<T>(string urlContent)
        {
            HttpResponseMessage responseMessage;
            /*if (Options.Timeout != TimeSpan.Zero)
            {
                this.httpClient.Timeout = Options.Timeout;
            }*/
            Uri uri = new Uri(this.httpClient.BaseAddress + urlContent);

            responseMessage = this.httpClient.GetAsync(uri).Result;

            try
            {
                responseMessage.EnsureSuccessStatusCode();
            }
            catch (Exception comException)
            {
                //log.Error(comException.ToString());
            }

            try
            {
                string responseText = responseMessage.Content.ReadAsStringAsync().Result;

                if (!string.IsNullOrEmpty(responseText))
                {
                    T convertedType = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText);

                    return convertedType;
                }
                else
                    return default(T);
            }
            catch (Exception ex)
            {
                /*log.Error(ex.ToString());
                log.Error("HTTPCLIENT URL: " + uri);
                log.Error("HTTPCLIENT STATUSCODE: " + responseMessage.StatusCode);*/
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>("");
            }
        }

        private async void GetImage(string strURL)
        {
            byte[] byteRespuesta;
            System.Net.Http.HttpClient client;
            try
            {
                client = new System.Net.Http.HttpClient();
                client.Timeout = TimeSpan.FromSeconds(30);
                byteRespuesta = await client.GetByteArrayAsync(strURL);
                if (byteRespuesta.Length > 0)
                {
                   //Salvar foto
                }
            }
            catch (Exception e)
            {
                
            }
        }

        public virtual void Dispose()
        {
            if (httpClient != null)
                httpClient.Dispose();
        }
    }
}
