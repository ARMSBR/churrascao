﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Churrascao.Classes
{
    public class ParticipantResponse
    {
        public int id;

        public string name;

        public bool confirmed;

        public double value_paid;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public bool Confirmed
        {
            get
            {
                return confirmed;
            }
            set
            {
                confirmed = value;
            }
        }

        public double Value
        {
            get
            {
                return value_paid;
            }
            set
            {
                value_paid = value;
            }
        }
    }
}
