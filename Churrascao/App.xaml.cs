﻿using System;
using Churrascao.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Churrascao
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
            //MainPage = new NavigationPage(new LoginView())
            MainPage = new NavigationPage(new TelaLoginView())
            {
                BarBackgroundColor = Color.FromHex("#ffc90e"),
                BarTextColor = Color.White,
            };
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
