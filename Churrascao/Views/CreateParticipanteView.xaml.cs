﻿using Churrascao.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Churrascao.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateParticipanteView : ContentPage
    {
        public CreateParticipanteView()
        {
            InitializeComponent();
            this.BindingContext = new CreateParticipanteViewModel(Navigation);
        }

        protected override void OnAppearing()
        {
            (BindingContext as CreateParticipanteViewModel).LoadParticipant();
            base.OnAppearing();
        }
    }
}