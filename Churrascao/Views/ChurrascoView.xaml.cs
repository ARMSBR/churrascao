﻿using Churrascao.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Churrascao.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChurrascoView : ContentPage
    {
        public ChurrascoView()
        {
            InitializeComponent();
            this.BindingContext = new ChurrascoViewModel(Navigation);
        }

        protected override void OnAppearing()
        {
            (BindingContext as ChurrascoViewModel).LoadChurrasco();
            base.OnAppearing();
        }
    }
}