﻿using Churrascao.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Churrascao.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TelaLoginView : ContentPage
    {
        public TelaLoginView()
        {
            InitializeComponent();
            this.BindingContext = new TelaLoginViewModel(Navigation);
        }

        async void OnBtnEntrarClicked(object sender, EventArgs args)
        {
            
        }
    }
}