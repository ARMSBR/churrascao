﻿using Churrascao.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Churrascao.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListaChurrascosView : ContentPage
    {
        public ListaChurrascosView()
        {
            InitializeComponent();
            this.BindingContext = new ListaChurrascosViewModel(Navigation);
        }

        protected override void OnAppearing()
        {
            (BindingContext as ListaChurrascosViewModel).LoadChurrascos();
            base.OnAppearing();
        }
    }
}