﻿using Foundation;
using Churrascao.Interfaces;
using Churrascao.iOS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(SystemIOS))]
namespace Churrascao.iOS
{
    public class SystemIOS : ISystem
    {
        public string System()
        {
            return "IOS";
        }
    }
}
