﻿using Foundation;
using Churrascao.Interfaces;
using Churrascao.iOS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(MensagemIOS))]
namespace Churrascao.iOS
{
    public class MensagemIOS : IMessages
    {
        public void DisplayMesssage(string title, string message, bool longLength = true)
        {
            Action ActionShowDisplayNativeTooltip = async () =>
            {
                await ShowMessage(title, message, longLength);
            };
            RunOnUiThread(ActionShowDisplayNativeTooltip);
        }

        private async Task ShowMessage(string title, string message, bool longLength = true)
        {
            int delayTime = longLength ? 2650 : 650;
            var rootController = UIApplication.SharedApplication.KeyWindow.RootViewController;

            var navcontroller = rootController as UINavigationController;

            if (navcontroller != null)
                rootController = navcontroller.VisibleViewController;

            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                UIAlertController okayAlertController = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);
                rootController.PresentViewController(okayAlertController, true, null);
                
                await Task.Delay(delayTime);
                rootController.DismissViewController(true, null);
            }
            else
            {
                UIAlertView alertView = new UIAlertView(null, message, null, null, null);
                alertView.Show();

                await Task.Delay(delayTime);
                alertView.DismissWithClickedButtonIndex(0, true);
            }
        }

        private async Task ShowNotification(string title, string message, string icon = null)
        {
            UILocalNotification notification = new UILocalNotification();
            NSDate.FromTimeIntervalSinceNow(15);
            notification.AlertTitle = title; // required for Apple Watch notifications
            notification.AlertAction = "OK";
            notification.AlertBody = message;
            notification.SoundName = UILocalNotification.DefaultSoundName;
            notification.FireDate = NSDate.FromTimeIntervalSinceNow(15);
            notification.HasAction = true;
            UIApplication.SharedApplication.PresentLocalNotificationNow(notification);
        }

        public void DisplayMesssage(string message, bool longLength = true)
        {
            Action ActionDisplayNativeTooltip = async () =>
            {
                await ShowMessage(string.Empty, message, longLength);
            };
            RunOnUiThread(ActionDisplayNativeTooltip);
        }

        public void RunOnUiThread(Action action)
        {
            Xamarin.Forms.Device.BeginInvokeOnMainThread(action);
        }

        public void DisplayNotification(string title, string message, string icon)
        {
            ShowNotification(title, message, icon);
        }

        public void DisplayNotification(string title, string message)
        {
            ShowNotification(title, message);
        }

        public void DisplayNotification(string message)
        {
            ShowNotification(string.Empty, message);
        }
    }
}
