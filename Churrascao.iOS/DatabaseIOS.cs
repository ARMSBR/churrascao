using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using SQLite;
using Churrascao.iOS;
using System.IO;
using Churrascao.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(DatabaseIOS))]
namespace Churrascao.iOS
{
    public class DatabaseIOS : IDatabase
    {
        public SQLiteConnection GetConnection()
        {
            string dbName = "Churrascao.db3";
            //var pathDB = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), dbName);
            var personalFolder = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal));
            var LibraryFolder = Path.Combine(personalFolder, "..", "Library");
            var pathDB = Path.Combine(LibraryFolder, dbName);

            return new SQLiteConnection(pathDB);
        }
    }
}