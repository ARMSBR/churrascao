using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;

namespace Churrascao.Droid
{
    [Activity(Label = "Churrascao",  Icon = "@drawable/share", Theme = "@style/Theme.Splash", MainLauncher = true, NoHistory = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait, WindowSoftInputMode = SoftInput.AdjustPan)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Android.OS.Bundle savedInstanceState)
        {
            int splashlayout = Resources.GetIdentifier("splashscreen", "layout", this.PackageName);

            SetContentView(splashlayout);

            var handler = new Handler();
            handler.PostDelayed(()=> BootApplication(savedInstanceState), 1000);
            
            base.OnCreate(savedInstanceState);
        }

        private void BootApplication(Android.OS.Bundle savedInstanceState)
        {
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState, this.GetType().Assembly);

            MainActivityDescriptor descriptor = new MainActivityDescriptor();

            string mainActivityName = descriptor.MainActivityName;
            string packageName = descriptor.PackageName;

            Intent i = new Intent();
            i.SetComponent(new Android.Content.ComponentName(packageName, mainActivityName));
            //i.AddFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop | ActivityFlags.ClearTask | ActivityFlags.NoAnimation);
            i.AddFlags(ActivityFlags.NoAnimation);

            StartActivityForResult(i, 0);
            OverridePendingTransition(0, 0); //0 for no animation

        }
    }
}