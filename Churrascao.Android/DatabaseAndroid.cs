using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Churrascao.Interfaces;
using Churrascao.Droid;
using SQLite;
using System.IO;

[assembly: Xamarin.Forms.Dependency(typeof(DatabaseAndroid))]
namespace Churrascao.Droid
{
    public class DatabaseAndroid : IDatabase
    {
        public SQLiteConnection GetConnection()
        {
            string dbName = "Churrascao.db3";
            var pathDB = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal) , dbName);

            return new SQLiteConnection(pathDB);
        }
    }
}