﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Churrascao.Interfaces;
using Churrascao.Droid;
using Android.Support.V4.App;

[assembly: Xamarin.Forms.Dependency(typeof(SystemDroid))]
namespace Churrascao.Droid
{
    public class SystemDroid : ISystem
    {
        public string System()
        {
            return "ANDROID";
        }
    }
}