﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Churrascao.Interfaces;
using Churrascao.Droid;
using Android.Support.V4.App;
using Android.Media;

[assembly: Xamarin.Forms.Dependency(typeof(MensagemDroid))]
namespace Churrascao.Droid
{
    public class MensagemDroid : IMessages
    {
        int count = 0;
        public void DisplayMesssage(string title, string message, bool longLength = true)
        {
            DisplayMesssage(message, longLength);
        }

        public void DisplayMesssage(string message, bool longLength = true)
        {
            try
            {                
                var toastLength = longLength ? ToastLength.Long : ToastLength.Short;
                var toast = Toast.MakeText(Xamarin.Forms.Forms.Context.ApplicationContext, message, toastLength);                
                toast.Show();
            }
            catch (Exception ex)
            {
                // log ?
            }
        }

        public void RunOnUiThread(Action action)
        {
            Xamarin.Forms.Device.BeginInvokeOnMainThread(action);
        }

        private void ShowNotification(string title, string message, string icon = null)
        {
            // Pass the current button press count value to the next activity:
            /*Bundle valuesForActivity = new Bundle();
            valuesForActivity.PutInt("count", count);

            // When the user clicks the notification, SecondActivity will start up.
            Intent resultIntent = new Intent(this, typeof(SecondActivity));

            // Pass some values to SecondActivity:
            resultIntent.PutExtras(valuesForActivity);

            // Construct a back stack for cross-task navigation:
            Android.Support.V4.App.TaskStackBuilder stackBuilder = Android.Support.V4.App.TaskStackBuilder.Create(this);
            stackBuilder.AddParentStack(Java.Lang.Class.FromType(typeof(SecondActivity)));
            stackBuilder.AddNextIntent(resultIntent);

            // Create the PendingIntent with the back stack:            
            PendingIntent resultPendingIntent =
                stackBuilder.GetPendingIntent(0, (int)PendingIntentFlags.UpdateCurrent);

            // Build the notification:
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .SetAutoCancel(true)                    // Dismiss from the notif. area when clicked
                .SetContentIntent(resultPendingIntent)  // Start 2nd activity when the intent is clicked.
                .SetContentTitle("Button Clicked")      // Set its title
                .SetNumber(count)                       // Display the count in the Content Info
                .SetSmallIcon(Resource.Drawable.ic_stat_button_click)  // Display this icon
                .SetContentText(String.Format(
                    "The button has been clicked {0} times.", count)); // The message to display.

            // Finally, publish the notification:
            NotificationManager notificationManager =
              (NotificationManager)GetSystemService(Context.NotificationService);
            notificationManager.Notify(ButtonClickNotificationId, builder.Build());*/


            // When the user clicks the notification, SecondActivity will start up.
            Intent resultIntent = new Intent(Application.Context, typeof(MainActivity));
            PendingIntent pendingIntent = PendingIntent.GetActivity(Application.Context, 0, resultIntent, PendingIntentFlags.OneShot);

            // Instantiate the builder and set notification elements:
            Notification.Builder builder = new Notification.Builder(Application.Context)
            .SetDefaults(NotificationDefaults.Sound | NotificationDefaults.Vibrate)
            .SetSound(RingtoneManager.GetDefaultUri(RingtoneType.Notification))
            .SetContentTitle((string.IsNullOrEmpty(title) ? "Alerta" : title))
            .SetContentText(message)
            .SetContentIntent(pendingIntent);
            //.SetSmallIcon(Resource.Drawable.ic_stat_button_click);
            builder.SetAutoCancel(true);
            builder.SetNumber(count);
            builder.SetPriority(1);


            // Build the notification:
            Notification notification = builder.Build();

            // Get the notification manager:
            NotificationManager notificationManager =
            Application.Context.GetSystemService(Context.NotificationService) as NotificationManager;

            // Publish the notification:
            const int notificationId = 0;
            notificationManager.Notify(notificationId, notification);

            builder.SetWhen(DateTime.Now.AddMilliseconds(100).Millisecond);
        }

        public void DisplayNotification(string title, string message, string icon)
        {
            ShowNotification(title, message, icon);
        }

        public void DisplayNotification(string title, string message)
        {
            ShowNotification(title, message);
        }

        public void DisplayNotification(string message)
        {
            ShowNotification(string.Empty, message);
        }
    }
}