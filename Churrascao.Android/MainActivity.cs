﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V4.App;
using AndroidX.Core.App;
using Android;
using Android.Content;
using Churrascao.Interfaces;
using Xamarin.Forms;
using Churrascao.Droid;

[assembly: Dependency(typeof(MainActivity))]
namespace Churrascao.Droid
{
    [Activity(Label = "Churrascao", Name = "com.churrascao.app.android.MainActivity", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity, INativeService
    {
        public static Context Context;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            Window.RequestFeature(WindowFeatures.NoTitle);
            base.OnCreate(savedInstanceState);
            Context = this;

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            LoadApplication(new App());
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public bool CheckPermissions()
        {
            var context = Context as MainActivity;
            Permission[] permissions = new Permission[]
            {
                ActivityCompat.CheckSelfPermission(context, Manifest.Permission.ReadExternalStorage),
                ActivityCompat.CheckSelfPermission(context, Manifest.Permission.WriteExternalStorage),
            };
            foreach (var permission in permissions)
            {
                if (permission == Permission.Denied)
                {
                    return false;
                }
            }
            return true;
        }
        public void RequestPermissions()
        {
            var context = Context as MainActivity;
            ActivityCompat.RequestPermissions(context, new string[]
            {
                Manifest.Permission.ReadExternalStorage,
                Manifest.Permission.WriteExternalStorage,
            }, 0);
        }
    }
}