using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Churrascao.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(MainActivityDescriptor))]

namespace Churrascao.Droid
{
    public class MainActivityDescriptor
    {
        public string MainActivityName
        {
            get
            {
                return "com.churrascao.app.android.MainActivity";
            }
        }

        public string PackageName
        {
            get
            {
                return "com.churrascao.app.android";
            }
        }
    }
}